import React from "react";
import { Link } from "react-router-dom";

function Navigation() {
  return (
    <nav>
      <p className="menu">Menu</p>
      <ul>
        <Link to="/Error">
          <img src="home-alt.png" alt="home"></img>Accueil
        </Link>
        <Link to="/Error">
          <img src="discovery.png" alt="home"></img>Découvrir
        </Link>
        <Link to="/Faq" className="tout">
          <img src="community.png" alt="home"></img>F.A.Q
        </Link>
      </ul>
    </nav>
  );
}

export default Navigation;

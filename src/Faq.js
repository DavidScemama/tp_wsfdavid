import "./Faq.css";
import { Link } from "react-router-dom";

function Faq() {
  return (
    <div class="tout">
      <Link to="/">
        <img className="arrow" src="arrow.png" alt="arrow"></img>
      </Link>
      <div class="items">
        <h1>faqs</h1>
        <details>
          <summary>Qui sommes-nous ?</summary>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum,
            reiciendis!
          </p>
        </details>
        <details>
          <summary>Qui sommes-nous ?</summary>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum,
            reiciendis!
          </p>
        </details>
        <details>
          <summary>Qui sommes-nous ?</summary>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum,
            reiciendis!
          </p>
        </details>
        <details>
          <summary>Qui sommes-nous ?</summary>
          <p>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum,
            reiciendis!
          </p>
        </details>
      </div>
    </div>
  );
}

export default Faq;

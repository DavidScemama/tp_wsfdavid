import "./App.css";
import Sidebar from "./Components/Sidebar/Sidebar.js";
import Error from "./Components/Error/Error.js";
import { Route, Routes } from "react-router-dom";
import Faq from "./Faq.js";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Sidebar />} />
        <Route path="*" element={<Error />} />
        <Route path="/Faq" element={<Faq />} />
      </Routes>
    </div>
  );
}

export default App;

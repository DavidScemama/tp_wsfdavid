import "./Sidebar.css";
import List from "../movieList/List.js";
// import Error from '../Error/Error.js';
import React, { useEffect, useState } from "react";
import axios from "axios";
import Navigation from "../../Navigation.js";

const Sidebar = () => {
  const [moviesData, setMoviesData] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=bcd8670cc4bfce2beaef588e3f92ef3d&query=${search}&language=fr-FR`
      )
      .then((res) => setMoviesData(res.data.results));
  }, [search]);

  return (
    <div className="container">
      <div className="navbar">
        <img className="logo" src="logo.png" alt="logo"></img>
        <div className="wrapper">
          <div className="searchBar">
            <input
              type="search"
              id="searchQueryInput"
              name="searchQueryInput"
              placeholder="Rechercher un film, une série..."
              onChange={(e) => setSearch(e.target.value)}
            />
            <button
              id="searchQuerySubmit"
              type="submit"
              name="searchQuerySubmit"
            >
              <img
                style={{ width: 24, height: 24 }}
                viewBox="0 0 24 24"
                src="search.svg"
                alt="search"
              ></img>
            </button>
          </div>
        </div>

        <Navigation />
      </div>
      <div className="result">
        {moviesData.map((movie) => (
          <List key={movie.id} movie={movie} />
        ))}
      </div>
    </div>
  );
};

export default Sidebar;

import "./Error.css";
import { Link } from "react-router-dom";

function Error() {
  return (
    <div className="all">
      <Link to="/">
        <img className="arrow" src="arrow.png" alt="arrow"></img>
      </Link>
      <div className="one" v>
        <div className="message">
          <p className="erreur">404</p>
          <p className="phrase">Cette page n'a pas encore été codé</p>
        </div>
      </div>
    </div>
  );
}

export default Error;
